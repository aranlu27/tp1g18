package ar.fiuba.tdd.tp1.model.expressions.binary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionSum extends ExpressionBinary {

    public ExpressionSum(Value opSum1, Value opSum2) {
        super(opSum1, opSum2);
    }

    @Override
    public ValueHolder evaluate() {
        return new ValueHolderImp(op1.getValue().getNumerical() + op2.getValue().getNumerical());
    }

}
