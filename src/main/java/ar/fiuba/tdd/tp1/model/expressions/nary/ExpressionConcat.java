package ar.fiuba.tdd.tp1.model.expressions.nary;

import ar.fiuba.tdd.tp1.model.value.Value;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.model.value.ValueHolderImp;

public class ExpressionConcat extends ExpressionNary {

    public ExpressionConcat(Value... ops) {
        super(ops);
    }

    @Override
    public ValueHolder evaluate() {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < ops.length; i++) {
            buf.append(ops[i].getValue().getAsString());
        }

        return new ValueHolderImp(buf.toString());
    }

}
