package ar.fiuba.tdd.tp1.model.value;

import ar.fiuba.tdd.tp1.extern.driver.BadFormulaException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ValueHolderImp implements ValueHolder {

    private static final String DDMMYYYY = "dd-MM-YYYY";
    private static final String MMDDYYYY = "MM-dd-YYYY";
    private static final String YYYYMMDD = "YYYY-MM-dd";
    
    
    boolean hasSettedString = false;
    boolean hasSettedNumber = false;
    boolean hasSettedDate = false;
    boolean hasSettedMoney = false;

    double number;
    String string;
    Date date;
    String formatter;
    String moneySymbol;
    private int decimals = 0;

    public ValueHolderImp(double number) {
        this.number = number;
        hasSettedNumber = true;
    }

    public ValueHolderImp(String string) {
        this.string = string;
        hasSettedString = true;
    }

    @Override
    public void setType(String type) throws ParseException {
        if (type.equals("Date")) {
            hasSettedString = false;
            hasSettedDate = true;
            formatter = YYYYMMDD;
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(this.string);
        } else if (type.equals("Currency")) {
            hasSettedNumber = false;
            hasSettedMoney = true;
            formatter = "U$S";
        } else {
            System.out.println("Error: invalid type");
        }
    }

    @Override
    public void setFormatter(String typeFormat, String formatter) throws ParseException {
        if (typeFormat.equals("format")) {
            validateDateFormat(formatter);
        } else if (typeFormat.equals("symbol")) {
            validateMoneySymbol(formatter);
        } else if (typeFormat.equals("decimal")) {
            validateDecimal(formatter);
        } else {
            System.out.println("Error: formatter '" + typeFormat + "' invalid");
        }
    }

    private void validateDateFormat(String formatter) throws ParseException {
        if (validDateFormat(formatter)) {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(this.string);
            this.formatter = formatter;
        } else {
            System.out.println("Error: invalid format for Date");
        }
    }

    private void validateMoneySymbol(String formatter) {
        if (validMoneySymbol(formatter)) {
            moneySymbol = formatter;
        } else {
            System.out.println("Error: invalid format for Symbol of Money");
        }
    }

    private void validateDecimal(String formatter) {
        if (validDecimalFormat(formatter)) {
            this.decimals = Integer.parseInt(formatter);
        } else {
            System.out.println("Error: invalid format for Decimal");
        }
    }

    private boolean validDecimalFormat(String decimals) {
        return decimals.equals("0") | decimals.equals("1") | decimals.equals("2");
    }

    private boolean validMoneySymbol(String symbol) {
        return symbol.equals("U$S") | symbol.equals("$") | symbol.equals("�");
    }

    private boolean validDateFormat(String format) {
        return format.equals(DDMMYYYY) | format.equals(YYYYMMDD) | format.equals(MMDDYYYY);
    }

    @Override
    public double getNumerical() {
        if (!hasSettedNumber) {
            throw new BadFormulaException();
        }
        return number;
    }


    @Override
    public String getLiteral() {
        if (!hasSettedString) {
            throw new BadFormulaException();
        }
        return string;
    }

    @Override
    public String getAsString() {
        if (hasSettedDate) {
            return new SimpleDateFormat(formatter).format(date);
        } else if (hasSettedMoney) {
            if (decimals > 0) {
                return this.moneySymbol + " " + String.format("%." + decimals + "f", this.number).replace(',', '.');
            }
            return this.moneySymbol + " " + (int) this.number;
        } else if (hasSettedNumber) {
            if (decimals > 0) {
                return String.format("%." + decimals + "f", this.number).replace(',', '.');
            }
            return String.valueOf((int)this.number);
        } else {
            return this.string;
        }
    }


}
