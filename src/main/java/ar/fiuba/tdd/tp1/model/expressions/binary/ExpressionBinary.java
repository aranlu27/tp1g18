package ar.fiuba.tdd.tp1.model.expressions.binary;

import ar.fiuba.tdd.tp1.model.expressions.ExpressionBase;
import ar.fiuba.tdd.tp1.model.value.Value;

import java.util.LinkedList;


public abstract class ExpressionBinary extends ExpressionBase {


    protected Value op1;
    protected Value op2;

    public ExpressionBinary(Value op1, Value op2) {
        listInvolved = new LinkedList<>();
        listInvolved.addAll(op1.getCellsInvolve());
        listInvolved.addAll(op2.getCellsInvolve());
        this.op1 = op1;
        this.op2 = op2;
    }


}
