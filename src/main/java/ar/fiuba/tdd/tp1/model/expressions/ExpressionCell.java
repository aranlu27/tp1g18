package ar.fiuba.tdd.tp1.model.expressions;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Demian on 03/10/2015.
 */
public class ExpressionCell implements Expression {

    Cell cell;

    public ExpressionCell(Cell cell) {
        this.cell = cell;
    }

    @Override
    public ValueHolder evaluate() {
        return cell.getValue();
    }

    @Override
    public ValueHolder getValue() {
        return cell.getValue();
    }

    @Override
    public List<Cell> getCellsInvolve() {
        LinkedList<Cell> list = new LinkedList<>();
        list.add(cell);
        return list;
    }
}
