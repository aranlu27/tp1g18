package ar.fiuba.tdd.tp1.model;

public class AddSpreedSheetAction implements Action {

    String spreedSheetName;
    private Document document;

    public AddSpreedSheetAction(Document document, String spreedSheetName) {
        this.document = document;
        this.spreedSheetName = spreedSheetName;
    }

    @Override
    public void redo() {
        this.document.redoSpreedSheet(spreedSheetName);
    }

    @Override
    public void undo() {
        this.document.undoSpreedSheet(spreedSheetName);
    }

}
