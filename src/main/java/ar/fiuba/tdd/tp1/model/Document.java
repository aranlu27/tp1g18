package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.value.ValueHolder;
import ar.fiuba.tdd.tp1.parser.ExpressionParser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Document implements Action {

    private static final String DEFAULT = "default";
    HashMap<String, SpreedSheet> spreedSheets;
    HashMap<String, SpreedSheet> spreedSheetsBackup;
    String documentName;
    ExpressionParser expressionParser;
    SpreedSheet currentSheet;
    private ActionHistory actionManager;

    public Document() {
        actionManager = new ActionHistory();
        spreedSheets = new HashMap<String, SpreedSheet>();
        spreedSheetsBackup = new HashMap<String, SpreedSheet>();
        currentSheet = new SpreedSheet(DEFAULT);
        spreedSheets.put(DEFAULT, currentSheet);
        expressionParser = new ExpressionParser(this);
    }

    public Document(String docName) {
        actionManager = new ActionHistory();
        spreedSheets = new HashMap<String, SpreedSheet>();
        spreedSheetsBackup = new HashMap<String, SpreedSheet>();
        currentSheet = new SpreedSheet(DEFAULT);
        spreedSheets.put(DEFAULT, currentSheet);
        this.documentName = docName;
        expressionParser = new ExpressionParser(this);
    }

    private void setSheet(String sheetName, SpreedSheet sheet) {
        currentSheet = sheet;
        spreedSheets.put(sheetName, sheet);
    }

    public SpreedSheet getSpreedSheet(String sheetName) {
        if (spreedSheets.containsKey(sheetName)) {
            return spreedSheets.get(sheetName);
        } else {
            setSheet(sheetName, new SpreedSheet(sheetName));
            return spreedSheets.get(sheetName);
        }
    }

    public String getDocumentName() {
        return this.documentName;
    }

    public Collection<SpreedSheet> getSpreedSheets() {
        return spreedSheets.values();
    }

    public List<String> getSpreedSheetsByName() {
        ArrayList<String> spreeds = new ArrayList<String>();
        spreeds.addAll(spreedSheets.keySet());
        return spreeds;

    }

    public void addDeserializerSheet(String spreedSheetName) {
        if (!spreedSheets.containsKey(spreedSheetName)) {
            spreedSheets.put(spreedSheetName, new SpreedSheet(spreedSheetName));
            this.currentSheet = spreedSheets.get(spreedSheetName); 
        }
    }

    public void addSpreedSheet(String spreedSheetName) {
        setSheet(spreedSheetName, new SpreedSheet(spreedSheetName));
        actionManager.setSpreedSheetAction(this, spreedSheetName);
    }

    public void undo() {
        actionManager.undo();
    }

    public void redo() {
        actionManager.redo();
    }

    public ValueHolder getCellValue(String pageName, String cellName) {
        return spreedSheets.get(pageName).get(cellName).getValue();
    }

    public void deserializeExpression(String actualSheetName, String actualCellName, String expression) {
        Expression expressionAnalized = expressionParser.parse(actualSheetName, expression);

        spreedSheets.get(actualSheetName).get(actualCellName).addExpressionAsString(expression);
        spreedSheets.get(actualSheetName).get(actualCellName).setExpression(expressionAnalized);
    }

    public void setValueInCurrentSheet(String cellName, String value) {
        deserializeExpression(currentSheet.getSpreedSheetName(), cellName, value);
    }

    public void setExpression(String actualSheetName, String actualCellName, String expression) {
        actionManager.setCellAction(spreedSheets.get(actualSheetName).get(actualCellName));

        Expression expressionAnalized = expressionParser.parse(actualSheetName, expression);

        spreedSheets.get(actualSheetName).get(actualCellName).addExpressionAsString(expression);
        spreedSheets.get(actualSheetName).get(actualCellName).addExpression(expressionAnalized);
        spreedSheets.get(actualSheetName).get(actualCellName).setExpression(expressionAnalized);
    }

    public Cell getCell(String sheetName, String cellName) {
        return getSpreedSheet(sheetName).get(cellName);
    }

    public void redoSpreedSheet(String spreedSheetName) {
        SpreedSheet spreedSheet = spreedSheetsBackup.remove(spreedSheetName);
        if (spreedSheet != null) {
            this.spreedSheets.put(spreedSheetName, spreedSheet);
        }
    }

    public void undoSpreedSheet(String spreedSheetName) {
        SpreedSheet spreedSheet = spreedSheets.remove(spreedSheetName);
        if (spreedSheet != null) {
            this.spreedSheetsBackup.put(spreedSheetName, spreedSheet);
        }
    }


    public void deleteDefault() {
        this.spreedSheets.remove(DEFAULT);
    }

    public SpreedSheet getCurrentSpreedSheet() {
        return currentSheet;
    }

    public void setCurrentSheet(String workSheetName) {
        this.currentSheet = this.spreedSheets.get(workSheetName);
    }
}
