package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.Stack;

public interface ParserOperand {
    boolean parse(String expression, Stack<Expression> operandStack, String current);
}
