package ar.fiuba.tdd.tp1.parser.resolver;

import ar.fiuba.tdd.tp1.model.expressions.Expression;
import ar.fiuba.tdd.tp1.model.expressions.ExpressionDefault;

public class NumberResolver extends TypeResolver {

    public NumberResolver(TypeResolver nextResolver) {
        super(nextResolver);
    }

    @Override
    protected Expression getExpression(String expression) {
        return new ExpressionDefault(Double.parseDouble(expression));
    }

    @Override
    protected boolean isThisType(String expression) {
        return patternFinded("(^-?\\ *[0-9]+\\ *$)|(^-?[0-9]+\\.[0-9]*\\ *$)", expression);
    }

}
