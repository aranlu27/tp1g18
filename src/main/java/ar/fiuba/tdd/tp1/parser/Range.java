package ar.fiuba.tdd.tp1.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Demian on 17/10/2015.
 */
public class Range {

    private int columnToInt(String column) {
        int columNumber = 0;

        for (int i = 0; i < column.length(); i++) {
            int current = (int) (column.charAt(i)) - 65;
            columNumber += current + i * 25;
        }
        return columNumber;
    }

    private String columnFromInt(int column) {
        int letterCount = column;
        StringBuffer columnBuf = new StringBuffer();
        while (letterCount >= 0) {
            int letter = letterCount % 25;
            letterCount -= 25;
            columnBuf.append((char) (letter + 65));
        }

        //TODO: Invertir orden, para una letra funciona pero apra AB tira BA

        return columnBuf.toString();
    }

    private String buildCellString(int column, int row) {
        String columnStr = columnFromInt(column);
        String rowStr = Integer.toString(row);

        return columnStr + rowStr;
    }

    public List<String> buildRange(String from, String to) {

        List<String> cellList = new ArrayList<String>();
        int topLeftColumn = columnToInt(getCellColumn(from));
        int topLeftRow = getCellRow(from);

        int botLeftColumn = columnToInt(getCellColumn(to));
        int botLeftRow = getCellRow(to);

        for (int row = topLeftRow; row <= botLeftRow; row++) {
            for (int col = topLeftColumn; col <= botLeftColumn; col++) {
                cellList.add(buildCellString(col, row));
            }
        }

        return cellList;
    }

    private String getCellColumn(String cell) {
        Pattern pat = Pattern.compile("^[A-Z]+");
        Matcher mat = pat.matcher(cell);

        if (mat.find()) {
            return mat.group(0);
        }

        throw new IllegalArgumentException();
    }

    private int getCellRow(String cell) {
        Pattern pat = Pattern.compile("[0-9]+$");
        Matcher mat = pat.matcher(cell);

        if (mat.find()) {
            try {
                return Integer.parseInt(mat.group(0));
            } catch (Exception e) {
                throw new IllegalArgumentException();
            }

        }
        throw new IllegalArgumentException();
    }

}
