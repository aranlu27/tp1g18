package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.model.expressions.Expression;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Demian on 16/10/2015.
 */
public abstract class ParenthesesMatchParser implements ParserOperand {
    protected String preExp = "";
    protected String posExp = "";
    protected String medExp = "\\(.*\\)";

    protected Parser subParser;

    ParenthesesMatchParser(Parser subParser) {
        this.subParser = subParser;
    }

    protected abstract boolean resolveInternalExpression(String expression, Stack<Expression> operandStack, String current);

    @Override
    public boolean parse(String expression, Stack<Expression> operandStack, String current) {

        if (canIParse(expression)) {
            return resolveInternalExpression(getSubExpresion(expression), operandStack, current);
        }

        return false;
    }

    private boolean canIParse(String expression) {
        Pattern pattern = Pattern.compile("^" + preExp + medExp + posExp + "$");
        Matcher match = pattern.matcher(expression);

        return match.find();
    }

    private String getSubExpresion(String expression) {
        Pattern pattern = Pattern.compile(medExp);
        Matcher match = pattern.matcher(expression);
        match.find();
        String subExpression = match.group(0);
        return subExpression.substring(1, subExpression.length() - 1);
    }
}
