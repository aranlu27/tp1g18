package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.model.Excel;

public interface Command {
    void execute(Excel exel,String[] args);
}
