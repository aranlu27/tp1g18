package ar.fiuba.tdd.tp1.persistance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ar.fiuba.tdd.tp1.deserialize.json.JsonDocumentDeserializer;
import ar.fiuba.tdd.tp1.model.Document;
import ar.fiuba.tdd.tp1.serialize.json.JsonDocumentSerializer;

import com.opencsv.CSVReader;

public abstract class Persister {
    protected final GsonBuilder gsonBuilder;
    protected final Gson gson;
    //    protected CSVWriter writerCSV;
    protected CSVReader readerCSV;

    public Persister() {
        gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(Document.class, new JsonDocumentDeserializer());
        gsonBuilder.registerTypeAdapter(Document.class, new JsonDocumentSerializer());

        gson = gsonBuilder.create();
    }
}
