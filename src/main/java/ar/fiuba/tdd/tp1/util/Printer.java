package ar.fiuba.tdd.tp1.util;

import ar.fiuba.tdd.tp1.model.Cell;
import ar.fiuba.tdd.tp1.model.SpreedSheet;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Printer {

    private static Printer instance;

    private Map<String, Cell> cellsWithValue;


    private Printer() {
        cellsWithValue = new HashMap<String, Cell>();

    }

    public static Printer getInstance() {
        if (instance == null) {
            instance = new Printer();
        }
        return instance;
    }

    public void printCSV(SpreedSheet sheet, final PrintWriter writer) {
        print(new Printable() {
            @Override
            public void print(String str) {
                writer.print(str);
                writer.flush();
            }
        }, sheet, "", ",", "");
    }

    public void printRepl(SpreedSheet sheet) {
        print(new Printable() {
            @Override
            public void print(String str) {
                System.out.print(str);
            }
        }, sheet, "0", " | ", "-");
    }

    class Maximus{
        int maxCol = 0;
        int maxRow = 0;
    }
    
    private void print(Printable printable, SpreedSheet sheet, String emptyVal, String separetor, String header) {
        cellsWithValue.clear();
        Collection<Cell> cells = sheet.getCells();
        
        Maximus max = new Maximus();
        
        for (Cell cell : cells) {
            //System.out.println(cell.getCellName() + ":" + cell.getValueAsString());
            CellNode cellNode = Helper.getCellNode(cell);

            checkMatrixRange(cellNode,max);
            
            cellsWithValue.put(cellNode.toString(), cell);
        }

        String value = "0.0";


        printHeaderAndFooter(printable, max.maxCol, separetor, value, header);

        for (int rows = 1; rows < max.maxRow + 1; rows++) {
            for (int cols = 1; cols < max.maxCol + 1; cols++) {

                String index = Helper.colAndRowToIndexString(cols, rows);
                
                value = setValue(emptyVal,index);
                
                printable.print(value);
                
                printSeparetor(cols,max.maxCol,separetor,printable);
                
            }
            printable.print("\n");
        }

        printHeaderAndFooter(printable, max.maxCol, separetor, value, header);


    }

    private void printSeparetor(int cols, int maxCol, String separetor, Printable printable) {
        if (cols != maxCol) {
            printable.print(separetor);
        }
    }

    private String setValue(String emptyVal, String index) {
        if (!cellsWithValue.containsKey(index)) {
            return emptyVal;
        } else {
            return cellsWithValue.get(index).getValueAsString();
        }
    }

    private void checkMatrixRange(CellNode cellNode, Maximus max) {
        if (cellNode.col > max.maxCol) {
            max.maxCol = cellNode.col;
        }
        if (cellNode.row > max.maxRow) {
            max.maxRow = cellNode.row;
        }
    }

    private void printHeaderAndFooter(Printable printable, int maxCol, String separetor, String value, String header) {
        for (int heads = 0; heads < maxCol; heads++) {
            for (int times = 0; times < separetor.length() + value.length(); times++) {
                printable.print(header);
            }
        }
        printable.print("\n");
    }

}
