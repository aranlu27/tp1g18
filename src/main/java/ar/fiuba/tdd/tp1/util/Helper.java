package ar.fiuba.tdd.tp1.util;

import ar.fiuba.tdd.tp1.model.Cell;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {


    public static CellNode getCellNode(Cell cell) {
        String name = cell.getCellName();

        String[] temp = separeteInLetterNum(name);
    
        int colNumber = getColNumberFromLetter(temp[0]);
        int rowNumber = Integer.parseInt(temp[1]);

        //empiezan de 1,1
        return new CellNode(rowNumber, colNumber);
    }

    private static int getColNumberFromLetter(String colname) {
        colname = colname.toLowerCase();
        int sum = 0;
        for (int i = 0; i < colname.length(); i++) {
            char ci = colname.charAt(i);
            int pos = ci - 'a' + 1;
            sum = sum * 26;
            sum += pos;
        }
        return sum;
    }

    private static String[] separeteInLetterNum(String name) {
        Pattern pattern = Pattern.compile("(?:(?![0-9]).)*");
        Matcher match = pattern.matcher(name);
        String colName = "";
        String numName = "";
        if (match.find()) {
            colName = match.group(0);
        }
        numName = name.replace(colName, "");
        return new String[]{colName, numName};
    }


    public static String colAndRowToIndexString(int col, int row) {
        return "" + col + ";" + row;
    }

}
