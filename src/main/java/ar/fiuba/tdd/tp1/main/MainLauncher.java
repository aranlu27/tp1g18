package ar.fiuba.tdd.tp1.main;

import ar.fiuba.tdd.tp1.model.Excel;
import ar.fiuba.tdd.tp1.repl.Command;
import ar.fiuba.tdd.tp1.repl.Repl;
import ar.fiuba.tdd.tp1.util.Printer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;


public class MainLauncher {
        
    public static void main(String[] arguments) throws Exception {
        
        Excel exel = new Excel();
        Repl repl = new Repl(exel);
        
        Reader inreader = new InputStreamReader(System.in);
        try {
            BufferedReader in = new BufferedReader(inreader);
                
            loop(in, repl, exel);
            
            
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loop(BufferedReader in, Repl repl, Excel exel) throws Exception {
        String command = null;
        String arg = "";
        String str = "";
        String[] args;
        Command currentCommand;
        while ((str = in.readLine()) != null) {
            arg = "";
            String[] tokens = str.split(" ");
            command = tokens[0];
            
            arg = str.replace(tokens[0] + " ", "");
            args = arg.split(",");
            
            currentCommand = repl.getCommand(command);
            excuteCommand(currentCommand,exel,args);
            
            
        }
    }

    private static void excuteCommand(Command com, Excel exel, String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        if (com != null) {
            
            com.execute(exel, args);
            
            Printer.getInstance().printRepl(exel.getCurrentSheet());
            PrintWriter writer = new PrintWriter("excelCSV.txt", "UTF-8");
            Printer.getInstance().printCSV(exel.getCurrentSheet(), writer);
            writer.close();
        }
    }
}
